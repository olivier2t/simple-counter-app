express = require("express"),
db = require('redis').createClient({
    host: '127.0.0.1',
    port: 6379
});

// Create server
var app = express();

app.get('/', function(req, res) {
    
    db.incr('visits', (err, reply) => {
        if (err) {
            console.log(err);
            res.status(500).send(err.message);
            return;
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end('<html><body><h1>Welcome to ManoMano !!</h1> <p>Visit #<strong>' + reply + '</strong></p></html></body>');
    });

});

app.listen(80);
console.log("Listening on 127.0.0.1");
